import logging
import os
import subprocess
import hashlib
import time
import io
import sys
import re


from flask import Flask, request, send_from_directory, jsonify, render_template, render_template_string, redirect, make_response
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from subprocess import PIPE


app = Flask(__name__)


UPLOAD_FOLDER = 'uploads'
EXTRACT_FOLDER = 'extracted'

if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)
if not os.path.exists(EXTRACT_FOLDER):
    os.makedirs(EXTRACT_FOLDER)

MAX_FILE_SIZE = 5 * 1024 * 1024  # 5 MB

logging.basicConfig(level=logging.INFO)


@app.route('/extracted/<path:filename>')
def serve_file(filename):
    full_path = os.path.join(EXTRACT_FOLDER, filename)

    if os.path.isdir(full_path):
        files = os.listdir(full_path)
        file_links = []


        for file in files:
            if os.path.isdir(os.path.join(full_path, file)):
                file_links.append((f"/extracted/{filename}/{file}/", file + '/'))
            else:
                file_links.append((f"/extracted/{filename}/{file}", file))

        return render_template("file_links.html", file_links=file_links)
    else:
        return send_from_directory(EXTRACT_FOLDER, filename, as_attachment=True)


@app.route('/tests/<string:filename>', methods=['GET'])
def test(filename):

    if re.search(r'[^\w\.]', filename):
            response = make_response("Invalid test file. Please try again.", 400)
            response.headers["Content-Type"] = "text/plain"
            return response

    full_path = os.path.join(app.root_path, "tests", filename)

    if not os.path.exists(full_path):
        response = make_response("test not found", 404)
        response.headers["Content-Type"] = "text/plain"
        return response  

    try:
        with open(full_path, 'rb') as f:
            content = f.read()
        content = content.replace(b'\x00', b'')
        with open(full_path, 'wb') as f:
            f.write(content)
        process = subprocess.Popen(["python", full_path], stdout=PIPE, stderr=PIPE)
        output, error = process.communicate()

        output = output.decode('utf-8').strip() if output else 'No output'
        error = error.decode('utf-8').strip() if error else 'No error'
        status_code = 500 if error != 'No error' else 200
        message = error if error != 'No error' else output
        resp = jsonify({"test error": message}) if error != 'No error' else jsonify({"test passed": message})

        response = make_response(resp, status_code)
        response.headers["Content-Type"] = "application/json"
        return response

    except Exception as e:
        response = make_response(jsonify({"message": "Error"}), 500)
        response.headers["Content-Type"] = "application/json"  
        return response


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/upload', methods=['POST'])
def upload():
        file = request.files['file']

        if not file or file.filename == '' or not file.filename.endswith('.rar') or re.search(r'[^\w\.]', file.filename):
            app.logger.warning(f"Suspicious file upload attempt from {request.remote_addr}")
            return jsonify({"error": "Invalid archive. Please try again."}), 400

        if file.content_length > MAX_FILE_SIZE:
            return 'File too large! Max size is 5 MB.', 413

        file_content = file.read()

        timestamp = str(int(time.time()))
        fileHash = hashlib.md5(file.filename.encode('utf-8')).hexdigest()
        unique_folder_name = f"{timestamp}_{fileHash}"

        filepath = os.path.join(UPLOAD_FOLDER, file.filename)
        with open(filepath, 'wb') as f_out:
            f_out.write(file_content)

        extract_path = os.path.join(EXTRACT_FOLDER, unique_folder_name)
        os.makedirs(extract_path, exist_ok=True)
        
        app.logger.info(extract_path)
        result = subprocess.run(["unrar", "x", os.path.join(app.root_path, filepath)], cwd=os.path.join(app.root_path, extract_path), capture_output=True, text=True)

        app.logger.info(result.stdout)
        app.logger.error(result.stderr)

        app.logger.info(f"File {file.filename} uploaded and extracted by {request.remote_addr}")
        return redirect(f"/extracted/{unique_folder_name}") 



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
