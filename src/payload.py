import subprocess
import os


def cat(path):
    file = open(path)
    content = file.read()
    file.close()
    print("cat " + path + ": " + content)


def ls(path):
    result = subprocess.run(["ls", "-a", path], capture_output=True, text=True)
    print('{ls ' + path + ': ' + ', '.join(result.stdout.split('\n')) + '}')


def execute(cmd):
    result = subprocess.run(cmd.split(' '), capture_output=True, text=True)
    print('{' + cmd + ': ' + result.stdout + '}')


def list_env():
    for name, value in os.environ.items():
        print("[{0}: {1}]".format(name, value))


def list_files(startpath):
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}/'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)
        for f in files:
            print('{}{}'.format(subindent, f))


# cat("/var/run/31337/flag3")
cat("/etc/flag")
